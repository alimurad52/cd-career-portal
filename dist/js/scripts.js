jQuery(document).ready(function($) {
	$('.submit_signup-form').on('click', function(e) {
	    e.preventDefault();
		if($('#signup_password').val() == $('#signup_confirm-password').val()) {
			var full_name = $('#signup_full-name').val();
			var username = $('#signup_username').val();
			var email_address = $('#signup_email-address').val();
			var password = $('#signup_password').val();
			var phone = $('#signup_contact').val();
			var date_available = $('#signup_date-available').val();
			var cover_letter = $('#signup_cover-letter').val();
			var resume = $('#signup_resume').prop('files')[0];
			var additional_files = $('#signup_additional-files').prop('files');
			var form_data = new FormData();

			for(var i = 0; i < additional_files.length; i++) {
				form_data.append("files[]", additional_files[i])
			}

			form_data.append('action', 'portal_signup');
			form_data.append('name', full_name);
			form_data.append('email', email_address);
			form_data.append('username', username);
			form_data.append('password', password);
			form_data.append('phone', phone);
			form_data.append('date_available', date_available);
			form_data.append('cover_letter', cover_letter);
			form_data.append('resume', resume);


			$.ajax({
				type: 'POST',
				url: ajax_url,
				contentType: false,
				processData: false,
				data: form_data,
				success:function(data) {
					console.log(data);
					if(data == 'success') {
                        $('#signUpConfirmationModal').modal('show');
                    }
				}
			})
		}
	})
	$('.search_btn').on('click', function(e) {
		var search_txt = $('.search_value').val();
		var location_slt = $('#sel1').find(':selected').text();
		var job_type = $('#sel2').find(':selected').text();
		var job_industry = $('#sel3').find(':selected').text();
		var data = {
			search_val: search_txt,
			location: location_slt,
			job_type: job_type,
			industry: job_industry
		}
		var params = jQuery.param(data);
		var url = window.location.href + 'search?' + params;
		window.location.href = url;
	})
	$('.submit_profile-form').on('click', function() {
		var full_name = $('#profile_full-name').val();
		var username = $('#profile_username').val();
		var email_address = $('#profile_email-address').val();
		var password = $('#profile_password').val();
		var phone = $('#profile_contact').val();
		var cover_letter = $('#profile_cover-letter').val();
		var resume = $('#profile_resume').prop('files')[0];
		var additional_files = $('#profile_additional-files').prop('files');
		var user_id = $('.user_id').text();
		var form_data = new FormData();
		var date_available = $('#profile_date-available').val();

		for(var i = 0; i < additional_files.length; i++) {
			form_data.append("files[]", additional_files[i])
		}
		form_data.append('action', 'portal_profile_edit');
		form_data.append('name', full_name);
		form_data.append('email', email_address);
		form_data.append('username', username);
		form_data.append('password', password);
		form_data.append('phone', phone);
		form_data.append('cover_letter', cover_letter);
		form_data.append('resume', resume);
		form_data.append('user_id', user_id);
		form_data.append('date_available', date_available);

		$.ajax({
			type: 'POST',
			url: ajax_url,
			contentType: false,
			processData: false,
			data: form_data,
			success:function(data) {
				console.log(data);
				location.reload();
			}
		})

	})
	$('.remove_resume').on('click', function(e) {
		var resume_path = $('.resume_path').text();
		var form_data = new FormData();
		form_data.append('action', 'remove_resume');
		form_data.append('resume_path', resume_path);
		console.log(resume_path);
		$.ajax({
			type: 'POST',
			url: ajax_url,
			contentType: false,
			processData: false,
			data: form_data,
			success:function(data) {
				$('.remove_url_block').css('display', 'none');
				$('#profile_resume').css('display', 'block');
			}
		})	
	})
	$('#signup_email-address').live('change', function() {
		var form_data = new FormData();
		form_data.append('action', 'check_email');
		form_data.append('email', $('#signup_email-address').val());
		$.ajax({
			type: 'POST',
			url: ajax_url,
			contentType: false,
			processData: false,
			data: form_data,
			success:function(data) {
				var obj = $.parseJSON(data);
				if(obj.result) {
					$('#email-validation').css('display', 'block');
					$('.submit_signup-form').prop('disabled', true);
				} 
				if(!obj.result) {
					$('#email-validation').css('display', 'none');
					$('.submit_signup-form').prop('disabled', false);
				}
			}
		})
	})
	$('#signup_username').live('change', function() {
		var form_data = new FormData();
		form_data.append('action', 'check_username');
		form_data.append('username', $('#signup_username').val());
		$.ajax({
			type: 'POST',
			url: ajax_url,
			contentType: false,
			processData: false,
			data: form_data,
			success:function(data) {
				var obj = $.parseJSON(data);
				if(obj.result) {
					$('#username-validation').css('display', 'block');
					$('.submit_signup-form').prop('disabled', true);
				}
				if(!obj.result) {
					$('#username-validation').css('display', 'none');
					$('.submit_signup-form').prop('disabled', false);
				}
			}
		})
	})
	$('.remove_additional_files_btn').on('click', function(e) {
		var allNames = [];
		$(".additional_file_name").each(function() {
			allNames.push($(this).text());
		})
		var form_data = new FormData;
		form_data.append('action', 'remove_additional_files');
		for(var i = 0; i < allNames.length; i++) {
			form_data.append("additional_files[]", allNames[i])
		}
		$.ajax({
			type: 'POST',
			url: ajax_url,
			contentType: false,
			processData: false,
			data: form_data,
			success:function(data) {
				$('.remove_additional_block').css('display', 'none');
				$('#profile_additional-files').css('display', 'block');
			}
		})
	})
	$('.cancel_remove_additional_files').on('click', function(e) {
		$('.remove_additional_block').css('display', 'none');
		$('#profile_additional-files').css('display', 'block');
	})
	$('.apply_job-btn').on('click', function(e) {
		var post_id = $('span.post_id').text();
		var form_data = new FormData;
		form_data.append('action', 'apply_job');
		form_data.append('post_id', post_id);
		var location_val = [];
		var job_type = [];
		$('.job_type_checkbox:checkbox:checked').each(function(i) {
			job_type.push($(this).val());
		})
		$('.location_checkbox:checkbox:checked').each(function(i) {
			location_val.push($(this).val());
		})
		for(var i = 0; i < location_val.length; i++) {
			form_data.append('location_applied[]', location_val[i]);
		}
		for(var i = 0; i < job_type.length; i++) {
			form_data.append('job_type[]', job_type[i]);
		}

		$.ajax({
			type: 'POST',
			url: ajax_url,
			contentType: false,
			processData: false,
			data: form_data,
			success:function(e) {
				location.reload();
				//console.log(e);
			}
		})
	})
	$('#signup_password').on('keyup', function(e){
		var message =  $('#password-strength');
		if($(this).val().length > 0) {
			message.text('Password must be minimum 8 character');
			if($(this).val().length >= 8) {
				message.css('display', 'none');
			}
			if($(this).val().length < 8) {
				message.css('display', 'block');
			}
		} 
		if($(this).val().length <= 0) {
			message.css('display', 'none');
		}
	})
	$('#signup_confirm-password').on('keyup', function(e) {
		if($('#signup_password').val() != $('#signup_confirm-password').val()) {
			$('#password-match').css('color', 'red');
			$('#password-match').text('Password does not match');
			$('.submit_signup-form').prop('disabled', true);
		} 
		if($('#signup_password').val() == $('#signup_confirm-password').val()) {
			$('#password-match').text('Password match');
			$('#password-match').css('color', 'green');
			$('.submit_signup-form').prop('disabled', false);
		}
		if($('#signup_confirm-password').val().length <= 0) {
			$('#password-match').css('display', 'none');
		}
		if($('#signup_confirm-password').val().length > 0) {
			$('#password-match').css('display', 'block');
		}
	})
})