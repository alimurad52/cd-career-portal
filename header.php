<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">
<head>
	<title>
		<?php bloginfo('title'); ?>
	</title>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php //<div id="page-container" class="container-fluid site"> ?>

	<header id="page-header">
		<?php if ( has_nav_menu('header-menu-left') || has_nav_menu('header-menu-right') )  : ?>
		<div class="nav-container"> <!-- part to declare if header is stretched or not -->
			<nav class="navbar navbar-default">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarResponsive" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button> 
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_site_url(); ?>/wp-content/themes/career_portal/src/img/icapital_logo.png" alt="nav-logo" class="navbar-logo"  /></a>

					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="navbarResponsive">
						<?php
						if ( has_nav_menu('header-menu-right') ) {
							wp_nav_menu( array(
								'theme_location' => 'header-menu-right',
								'container' => 'ul',
								'menu_id' => 'header-menu-right',
								'menu_class'     => 'nav navbar-nav pull-right',
							) );
						}
                        echo '<li class="forum_link"><a href="'.get_site_url().'/forums">Forum</a>';
						?>
						<?php
						if(is_user_logged_in()) {
							if(current_user_can('administrator')) {
								echo '<li class="applicants_admin"><a href="'.get_site_url().'/applications">Applications</a></li> ';
							}
						}
						if(is_user_logged_in()) {
							echo '<li class="profile-menu-item"><a href="'.get_site_url().'/profile"><i class="fas fa-user-circle"></i>  Hello, '.get_user_meta(wp_get_current_user()->ID, "_signup_name", true).'</a></li>';
						}
						?>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>

		</div>
	<?php endif; ?>
</header>

<?php if ( is_active_sidebar( 'topwidget' ) ) : ?>
	<div class="container">
		<div class="row tb-highlight">
			<?php dynamic_sidebar( 'topwidget' ); ?>
		</div>
	</div>
<?php endif; ?>


<div id="site-content" role="content" >


