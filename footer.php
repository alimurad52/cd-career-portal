
</div>

<footer id="site-footer" class="container">
	<?php if (is_active_sidebar('footer-1') || is_active_sidebar('footer-2') || is_active_sidebar('footer-3') || is_active_sidebar('footer-4')) : ?>
		<div class="row">
			<?php dynamic_sidebar( 'footer-1' ); ?>
			<?php dynamic_sidebar( 'footer-2' ); ?>
			<?php dynamic_sidebar( 'footer-3' ); ?>
			<?php dynamic_sidebar( 'footer-4' ); ?>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<?php echo (esc_html(get_theme_mod('footer_copyright', '')) != '') ? ("<p>".esc_html(get_theme_mod('footer_copyright'))."</p>") : (""); ?>
			<?php echo (esc_html(get_theme_mod('footer_addinfo', '')) != '') ? ("<p>".esc_html(get_theme_mod('footer_addinfo'))."</p>") : (""); ?>
		</div>
		<div class="col-xs-12 col-sm-6">
			<?php
			if ( has_nav_menu('footer-menu') ) {
				wp_nav_menu( array(
					'theme_location' => 'footer-menu',
					'container' => 'ul',
					'menu_id' => 'site-navigation',
					'menu_class'     => 'list-inline list-unstyled pull-right',
				) );
			}
			?>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>