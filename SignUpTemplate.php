	<?php /* Template Name: SignUpTemplate */ 

get_header(); ?>
	<div class="container">
		<form role="form" class="signup-form">
			<h1>Sign Up</h1>
			<div class="form-group">
				<label>Full Name</label>
				<input type="text" placeholder="Full Name" id="signup_full-name" class="form-control" required>
			</div>
			<div class="form-group">
				<label>Username</label>
				<input type="text" placeholder="Username" id="signup_username" class="form-control" required>
				<small id="username-validation">Username already exists.</small>
			</div>
			<div class="form-group">
				<label>Email Address</label>
				<input type="email" placeholder="Email Address" id="signup_email-address" class="form-control" required>
				<small id="email-validation">Email already exists.</small>
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" placeholder="Password" id="signup_password" class="form-control" required>
				<small id="password-strength">Weak</small>
			</div>
			<div class="form-group">
				<label>Confirm Password</label>
				<input type="password" placeholder="Cofirm Password" id="signup_confirm-password" class="form-control" required>
				<small id="password-match">Password does not match.</small>
			</div>
			<div class="form-group">
				<label>Phone</label>
				<input type="number" placeholder="0123456789" class="form-control" id="signup_contact" required>
			</div>
			<div class="form-group">
				<label>When are you available for work?</label>
				<input type="date" placeholder="" class="form-control" id="signup_date-available" required>
			</div>
			<div class="form-group">
				<label>Cover Letter</label>
				<textarea rows="20" class="form-control" id="signup_cover-letter"></textarea>
			</div>
			<div class="form-group">
				<label>Resume</label >
				<input type="file" id="signup_resume" accept="image/*, .pdf, .doc, .docx" required>
                <small>Accepted file types: .pdf, .doc, .docx and images</small>
			</div>
			<div class="form-group">
				<label>Additional files</label>
				<input type="file" id="signup_additional-files" accept="image/*, .pdf, .doc, .docx" multiple>
                <small>Accepted file types: .pdf, .doc, .docx and images</small>
			</div>
			<input type="submit" value="Submit" class="btn_portal-main submit_signup-form" disabled>
		</form>
	</div>
<?php get_footer(); ?>

    <div id="signUpConfirmationModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="location.reload()">&times;</button>
                    <h4 class="modal-title">Sign Up Successful</h4>
                </div>
                <div class="modal-body">
                    <p>A confirmation email has been sent. Please verify your email by clicking on the link provided in the email.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload()">OK</button>
                </div>
            </div>

        </div>
    </div>
