<?php
/**
 * Main
 *
 * @package career_portal
 */

get_header(); ?>

<main id="primary" class="container-fluid">
	<?php if ( have_posts() ) : ?>
		<div class="container">
			<div class="search_box">
				<div class="row">
					<input type="text" class="search_value" placeholder="Search job title...">
					<button class="search_btn" ><i class="fas fa-search"></i></button>
				</div>
				<div class="row">
					<div class="form-group col-sm-4">
						<label for="sel1">Location:</label>
						<select class="form-control" id="sel1">
							<option>Select Job Location</option>
							<?php  
							$locations_value = array('Kuala Lumpur', 'Sydney', 'Singapore', 'Shanghai', 'Hong Kong');
							foreach ($locations_value as $key => $value) {
								echo '<option style="background-color: white; color: black;">'.$value.'</option>';
							}
							?>
						</select>
					</div>
					<div class="form-group col-sm-4">
						<label for="sel2">Job Type:</label>
						<select class="form-control" id="sel2">
							<option>Select Job Type</option>
							<?php  
							$job_type_values = array('Full Time', 'Part Time', 'Internship');
							foreach ($job_type_values as $key => $value) {
								echo '<option>'.$value.'</option>';
							}
							?>

						</select>
					</div>
					<div class="form-group col-sm-4">
						<label for="sel3">Department:</label>
						<select class="form-control" id="sel3">
							<option>Select Department</option>
							<?php  
							$job_industry_values = array('Investment Research & Analysis', 'CEO\'s Office', 'Fund Management', 'Legal & Compliance', 'Management', 'Editorial', 'Marketing & Business Development', 'Human Resources', 'Accounting & Finance', 'Fund Administration', 'Computer Science & IT', 'Design & Creative Arts', 'Internship Opportunities');
							foreach ($job_industry_values as $key => $value) {
								echo '<option>'.$value.'</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>
			<?php 
			$args = array(
				'posts_per_page'   => 1000,
				'offset'           => 0,
				'category'         => '',
				'category_name'    => '',
				'orderby'          => 'date',
				'order'            => 'DESC',
				'include'          => '',
				'exclude'          => '',
				'meta_key'         => '',
				'meta_value'       => '',
				'post_type'        => 'post',
				'post_mime_type'   => '',
				'post_parent'      => '',
				'author_name'	   => '',
				'post_status'      => 'publish',
				'suppress_filters' => true
			);
			$posts_array = get_posts($args);
			foreach($posts_array as $post) {
				?><div class="list-group" id="post-<?php echo $post->ID; ?>">
					<span class="list-group-item">
						<h4 class="list-group-item-heading"><?php echo $post->post_title; ?></h4>
						<span class="col-sm-3 job-category"><i class="fas fa-building font-awesome"></i>  <?php echo get_post_meta($post->ID, "dropdown_meta_box_job_industry", true);?></span>
						<?php
						$locations = get_post_meta($post->ID, 'country_job_posting_', true);
						$location_str = '';
						$x = 0;
						foreach ($locations as $key) {
							if($x == 0) {
								$location_str = $location_str.$key;
							} else {
								$location_str = $location_str.', '.$key;
							}
							$x++;
						}
						?>
						<span class="col-sm-3 job-location"><i class="fas fa-map-marker font-awesome"></i>  <?php echo $location_str;?></span>
						<?php
						$job_type = get_post_meta($post->ID, 'job_type_', true);
						$job_type_str = '';
						$x = 0;
						foreach ($job_type as $key) {
							if($x == 0) {
								$job_type_str = $job_type_str.$key;
							} else {
								$job_type_str = $job_type_str.', '.$key;
							}
							$x++;
						}
						?>
						<span class="col-sm-3 job-type"><i class="fas fa-briefcase font-awesome"></i>  <?php echo $job_type_str;?></span>
						<span class="col-sm-3"><i class="far fa-calendar-check font-awesome"></i> <span class=" job-post">
							<?php 
							$date = $post->post_date;
							$formatted = date('d M Y', strtotime($date));
							echo "Posted on ".$formatted;
							?>
						</span></span>
						<div class="list-group-item-text"><p><?php echo substr($post->post_content, 0, 250)."..."; ?></p></div>
						<a href="<?php echo get_post_permalink($post->ID); ?>"><input type="button" class="btn_portal-main" value="Read More"></a>
					</span>
				</div>
				<?php
			}
			?>	
		</div>
	<?php endif;?>
</main>

<?php get_footer(); ?>