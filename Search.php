<?php /* Template Name: Search */ 
get_header(); ?>
<div class="container">
	<h4>Search Result</h4>
	<?php
	$job_location = $_GET['location'];
	$job_type = $_GET['job_type'];
	$job_industry = $_GET['industry'];
	$search_txt = $_GET['search_val'];
	if($job_location == "Select Job Location") {
		$job_location = '';
	}
	if($job_type == "Select Job Type") {
		$job_type = '';
	}
	if($job_industry == "Select Job Industry") {
		$job_industry = '';
	}
	$args = array(
		'post_type' => 'post',
		'posts_per_page'   => 1000,
		"s" => $search_txt,
		'meta_query' => [
			'relation' => 'AND',
			[
				array(
					'key' => 'country_job_posting_',
					'value' => $job_location,
					'compare' => 'LIKE',
				)
			],
			[
				array(
					'key' => 'job_type_',
					'value' => $job_type,
					'compare' => 'LIKE',
				)
			],
			[
				'key' => 'dropdown_meta_box_job_industry',
				'value' => $job_industry,
				'compare' => 'LIKE'
			],
		]
	);
	$posts = get_posts($args);
	if(empty($posts)) {
		echo '<span class="no-result">Sorry, no jobs with those specifications available.</span>';
	}
	foreach ($posts as $post) {
		?><div class="list-group" id="post-<?php echo $post->ID; ?>">
			<span class="list-group-item">
				<h4 class="list-group-item-heading"><?php echo $post->post_title; ?></h4>
				<span class="col-sm-3 job-category"><i class="fas fa-building font-awesome"></i>  <?php echo get_post_meta($post->ID, "dropdown_meta_box_job_industry", true);?></span>
				<?php
					$locations = get_post_meta($post->ID, 'country_job_posting_', true);
					$location_str = '';
					$x = 0;
					foreach ($locations as $key) {
						if($x == 0) {
							$location_str = $location_str.$key;
						} else {
							$location_str = $location_str.', '.$key;
						}
						$x++;
					}
				?>
				<span class="col-sm-3 job-location"><i class="fas fa-map-marker font-awesome"></i>  <?php echo $location_str; ?></span>
				<?php
					$job_type = get_post_meta($post->ID, 'job_type_', true);
					$job_type_str = '';
					$x = 0;
					foreach ($job_type as $key) {
						if($x == 0) {
							$job_type_str = $job_type_str.$key;
						} else {
							$job_type_str = $job_type_str.', '.$key;
						}
						$x++;
					}
				?>
				<span class="col-sm-3 job-type"><i class="fas fa-briefcase font-awesome"></i>  <?php echo $job_type_str;?></span>
				<span class="col-sm-3"><i class="far fa-calendar-check font-awesome"></i> <span class=" job-post">
					<?php 
					$date = $post->post_date;
					$formatted = date('d M Y', strtotime($date));
					echo $formatted;
					?>
				</span></span>
				<div class="list-group-item-text"><p><?php echo substr($post->post_content, 0, 250)."..."; ?></p></div>
				<a href="<?php echo get_post_permalink($post->ID); ?>"><input type="button" class="btn_portal-main" value="Read More"></a>
			</span>
		</div>
		<?php
	}
	?>
</div>

<?php get_footer(); ?>