<?php /* Template Name: Download */ 
if(isset($_GET["username"])) {
	$username = $_GET["username"];
	$scanDir = scandir(wp_upload_dir()['path']);
	$file_name = wp_get_current_user()->user_login."_".wp_get_current_user()->ID.'_additional_files_';
	$bool = false;
	$files = array();
	foreach ($scanDir as $file) {
		if(strlen($file) > 15) {
			if(strpos($file, $username.'_'.wp_get_current_user()->ID) !== false) {
				$file = wp_upload_dir()['path'].'/'.$file;
				array_push($files, $file);
			}
		}
	}
	$zipFile = $username.'_files.zip';
	create_zip($files, $zipFile);
}
function create_zip($files = array(),$destination = '',$overwrite = false) {
    $valid_files = array();
    if(is_array($files)) {
        foreach($files as $file) {
            if(file_exists($file)) {
                $valid_files[] = $file;
            }
        }
    }
    if(count($valid_files)) {
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            echo "failed";
            return false;
        }
        foreach($valid_files as $file) {
            echo $file;
            $zip->addFile($file,$file); 
        }
        $zip->close();
        header("Content-disposition: attachment; filename=$destination");
    	header("Content-type: application/zip");
    	ob_clean();
    	flush();
    	readfile($destination);
    	unlink($destination);
        return file_exists($destination);
    }
}
?>
<?php get_footer(); ?>