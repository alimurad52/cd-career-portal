	<?php /* Template Name: Applications */ 

	get_header(); ?>
	<?php 
	if(is_user_logged_in()) {
		if(current_user_can('administrator')) {
			?>
			<div class="container">
				<h3>Applications</h3>
				<?php $args = array(
					'posts_per_page'   => 1000,
					'offset'           => 0,
					'category'         => '',
					'category_name'    => '',
					'orderby'          => 'date',
					'order'            => 'DESC',
					'include'          => '',
					'exclude'          => '',
					'meta_key'         => '',
					'meta_value'       => '',
					'post_type'        => 'post',
					'post_mime_type'   => '',
					'post_parent'      => '',
					'author'	   => '',
					'author_name'	   => '',
					'post_status'      => 'publish',
					'suppress_filters' => true,
					'fields'           => '',
				);
				$posts_array = get_posts( $args );
				?>
                <div id="application_content">
				<?php
				foreach($posts_array as $post) {
					$has_apps = get_post_meta($post->ID, '_post_has_applications', true);
					if($has_apps) {
						echo '<br /> <h6 style="margin-top: 15px; clear: both;" class="pull-left">'.$post->post_title.'</h6>';
					}
					?>
					<?php
					$allUserIDs = get_post_meta($post->ID, '_applied_job_');
					echo '<span class="col-md-2 pull-left" style="clear:both;">';
					foreach ($allUserIDs as $user_id) {
						$username = get_userdata($user_id)->user_login;
						echo '<a class="download_profile" href="download?username='.$username.'"><li style="list-style:none;">'.get_user_meta($user_id, '_signup_name')[0].'</li></a>';
					}
					echo '</span>';
					?>
					<span class="col-md-2"><?php
					foreach ($allUserIDs as $user_id) {
						echo '<li class="'.get_user_meta($user_id, '_signup_name')[0].'-download_profile_id" style="list-style: none;">'.$user_id.'</li>';
					}
					?></span>
					<span class="col-md-2">
					<?php
					$locations_applied = get_post_meta($post->ID, '_locations_applied', true);
					if($locations_applied) {
						foreach ($locations_applied as $key) {
							echo '<li class="'.get_user_meta($user_id, '_signup_name')[0].'-locations_applied" style="list-style: none;">'.$key.'</li>';
						}
					}
					?>
					</span>
					<span class="col-md-2">
					<?php
					$job_type = get_post_meta($post->ID, '_job_type_applied', true);
					if($job_type) {
						foreach ($job_type as $key) {
							echo '<li class="'.get_user_meta($user_id, '_signup_name')[0].'-job_type_applied" style="list-style: none;">'.$key.'</li>';
						}
					}
					?>
					</span>
					<?php
					$date_applied = get_post_meta($post->ID, '_applied_job_date_');
					foreach ($date_applied as $date) {
						echo '<span class="col-md-3">'.$date.'</span>';
					}
				}
				?>
                </div>
			</div>
			<?php 
		}
	} ?>
	<?php get_footer(); ?>