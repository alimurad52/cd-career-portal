<?php /* Template Name: Profile */ 
get_header(); ?>
	<div class="container">
		<h2>Profile</h2>
		<form role="form" class="profile-form">
			<label class="hidden user_id" style="display: none;"><?php echo wp_get_current_user()->ID ?></label>
			<div class="form-group">
				<label>Full Name</label>
				<input type="text" placeholder="Full Name" id="profile_full-name" class="form-control" value="<?php echo get_user_meta(wp_get_current_user()->ID, "_signup_name", true)?>" required>
			</div>
			<div class="form-group">
				<label>Username</label>
				<input type="text" placeholder="Username" id="profile_username" class="form-control" value="<?php echo wp_get_current_user()->user_login ?>" readonly>
			</div>
			<div class="form-group">
				<label>Email Address</label>
				<input type="email" placeholder="Email Address" id="profile_email-address" class="form-control" value="<?php echo wp_get_current_user()->user_email ?>" required>
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" placeholder="Password" id="profile_password" class="form-control" >
				<small id="password-strength">Weak</small>
			</div>
			<div class="form-group">
				<label>Confirm Password</label>
				<input type="password" placeholder="Cofirm Password" id="profile_confirm-password" class="form-control" >
				<small id="password-match">Password does not match.</small>
			</div>
			<div class="form-group">
				<label>Phone</label>
				<input type="number" placeholder="0123456789" class="form-control" id="profile_contact" value="<?php echo get_user_meta(wp_get_current_user()->ID, "_signup_phone", true)?>" required>
			</div>
			<div class="form-group">
				<label>When are you available for work?</label>
				<input type="date" class="form-control" id="profile_date-available" value="<?php echo get_user_meta(wp_get_current_user()->ID, "_signup_date_available", true)?>" required>
			</div>
			<div class="form-group">
				<label>Cover Letter</label>
				<textarea rows="20" class="form-control" id="profile_cover-letter"><?php echo get_user_meta(wp_get_current_user()->ID, "_signup_cover_letter", true) ?> </textarea>
			</div>
			<div class="form-group">
				<label>Resume</label >
				<?php 
				$scanDir = scandir(wp_upload_dir()['path']);
				$file_name = wp_get_current_user()->user_login."_".wp_get_current_user()->ID.'_resume_';
				$resume_file;
				$bool = false;
				foreach($scanDir as $file) {
					if(stripos($file, $file_name) !== false) {
						$resume_file = $file;
						$bool = true;
					}
				}
				if($bool) {
					echo '<div class="remove_url_block"><label class="hidden resume_path">'.$resume_file.'</label>';
					echo '<label class="lbl_of_resume_exists">One resume exists. Remove and add new?</label>';
					echo '<br /><input type="button" value="Remove" class="btn_inverse remove_resume" style="height: 20px;"></div>';
					echo '<input type="file" id="profile_resume" accept="image/*, .pdf, .doc, .docx" style="display: none;" required>';
					echo '<small>Accepted file types: .pdf, .doc, .docx and images</small>';
				}  else {
					echo '<input type="file" id="profile_resume" accept="image/*, .pdf, .doc, .docx" required>';
					echo '<small>Accepted file types: .pdf, .doc, .docx and images</small>';
				}
				?>

			</div>
			<div class="form-group">
				<label>Additional files</label>
				<?php 
				$scanDir = scandir(wp_upload_dir()['path']);
				$file_name = wp_get_current_user()->user_login."_".wp_get_current_user()->ID.'_additional_files_';
				$bool = false;
				$files = array();
				foreach($scanDir as $file) {
					if(stripos($file, $file_name) !== false) {
						echo '<label class="additional_file_name hidden">'.$file.'</label>';
						$bool = true;
						array_push($files, $file);
					} 
				}
				if($bool) {
					echo '<div class="remove_additional_block">';
					echo '<label>'.count($files).' Additional files exist. Remove all? Click cancel to keep and add additional files.</label>';
					echo '<br /><input type="button" class="btn_inverse remove_additional_files_btn" style="height: 20px;" value="Remove" >';
					echo '<input type="button" class="btn_portal-inverse cancel_remove_additional_files" value="Cancel" style="height:20px; margin-left: 10px;"></div>';
					echo '<input type="file" id="profile_additional-files" accept="image/*, .pdf, .doc, .docx" style="display:none;" multiple>';
					echo '<small>Accepted file types: .pdf, .doc, .docx and images</small>';
				} else {
					echo '<input type="file" id="profile_additional-files" accept="image/*, .pdf, .doc, .docx" multiple>';
					echo '<small>Accepted file types: .pdf, .doc, .docx and images</small>';
				}
				?>
				
			</div>
			<input type="button" value="Save" class="btn_portal-main submit_profile-form">
		</form>
	</div>
<?php get_footer(); ?>