<?php
/**
 * Single
 *
 * @package thirdbird
 */

get_header(); ?>

<div class="container-fluid">
	<section id="single" class="boxed">

		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<h2 class="list-group-item-heading"><?php echo the_title(); ?></h2>
					<span class="col-sm-3 job-category"><i class="fas fa-building font-awesome"></i>  <?php echo get_post_meta($post->ID, "dropdown_meta_box_job_industry", true);?></span>
					<?php
					$locations = get_post_meta($post->ID, 'country_job_posting_', true);
					$location_str = '';
					$x = 0;
					foreach ($locations as $key) {
						if($x == 0) {
							$location_str = $location_str.$key;
						} else {
							$location_str = $location_str.', '.$key;
						}
						$x++;
					}
					?>
					<span class="col-sm-3 job-location"><i class="fas fa-map-marker font-awesome"></i>  <?php echo $location_str; ?></span>
					<?php
					$job_type = get_post_meta($post->ID, 'job_type_', true);
					$job_type_str = '';
					$x = 0;
					foreach ($job_type as $key) {
						if($x == 0) {
							$job_type_str = $job_type_str.$key;
						} else {
							$job_type_str = $job_type_str.', '.$key;
						}
						$x++;
					}
					?>
					<span class="col-sm-3 job-type"><i class="fas fa-briefcase font-awesome"></i>  <?php echo $job_type_str;?></span>
					<span class="col-sm-3"><i class="far fa-calendar-check font-awesome"></i> <span class=" job-post">
						<?php 
						$date = $post->post_date;
						$formatted = date('d M Y', strtotime($date));
						echo 'Posted on '.$formatted;
						?>
					</span></span>
					
					<div class="list-group-item-text"><p><?php echo the_content(); ?></p></div>
					<h6 style="color: #345b92; margin-top: 20px;">About the Position</h6>
					<br >
					<?php
					$job_level = get_post_meta($post->ID, 'job_level_', true);
					$job_level_str = '';
					$x = 0;
					foreach ($job_level as $key) {
						if($x == 0) {
							$job_level_str = $job_level_str.$key;
						} else {
							$job_level_str = $job_level_str.', '.$key;
						}
						$x++;
					}
					?>
					<table class="table">
						<tbody>
							<tr>
								<td>Job Level</td>
								<td><?php echo $job_level_str; ?></td>
							</tr>
							<?php if(get_post_meta($post->ID, "job_salary_meta_box", true)) { ?>
							<tr>
								<td>Salary</td>
								<td><?php echo get_post_meta($post->ID, "job_salary_meta_box", true);?></td>
							</tr>
							<?php } ?>
							<?php if(get_post_meta($post->ID, "job_id_meta_box", true)) { ?>
							<tr>
								<td>Job ID</td>
								<td><?php echo get_post_meta($post->ID, "job_id_meta_box", true); ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
					<footer class="">
						<?php 
						$user_id_array = get_post_meta($post->ID, '_applied_job_');
						if(is_user_logged_in()) {
							if(in_array(get_current_user_id(), $user_id_array)) {
								echo '<span class="job_applied"><i class="fas fa-check-circle"></i> Applied</span>';
							} else {
								echo '<div class="row">';
								echo '<div class="col-sm-4"><h6 style="text-align:left;" >Choose your location</h6>';
								echo '<div class="checkbox">';
								$locations = get_post_meta($post->ID, 'country_job_posting_', true);
								foreach ($locations as $location) {
									echo '<label style="text-align: left; line-height: 30px;" ><input class="location_checkbox" type="checkbox" value="'.$location.'">'.$location.'</label>';
								}
								echo '</div></div>';
								echo '<div class="col-sm-4"><h6 style="text-align:left;">Choose your preference</h6> ';
								echo '<div class="checkbox">';
								$job_types = get_post_meta($post->ID, 'job_type_', true);
								foreach ($job_types as $job_type) {
									echo '<label style="text-align: left; line-height: 30px;" ><input class="job_type_checkbox" type="checkbox" value="'.$job_type.'">'.$job_type.'</label>';
								}
								echo '</div></div>';
								echo '<div style="width: 100%;"><input type="button" class="btn_portal-main apply_job-btn" value="Apply" ></div>';
							}
						} else {
							echo '<a href="'.get_site_url().'/sign-up"><input type="button" class="btn_portal-main" value="Sign Up and Apply" ></a>';
							echo '<br><a href="'.wp_login_url(get_permalink()).'">Have an account? Log In</a>';
						}
						?>
						<span class="hidden post_id"><?php echo the_ID(); ?></span>
						<?php edit_post_link( __( 'Edit' ), '<span class="edit-link">', '</span><!-- .entry-footer -->' ); ?>
					</footer>
				</article>
			<?php endwhile;  // LOOP END?>
		<?php endif; ?>
	</section>
</div>

<?php get_footer(); ?>